-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-06-2016 a las 18:48:21
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `concesionario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesorios`
--

CREATE TABLE `accesorios` (
  `AccesorioID` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL DEFAULT '''''',
  `PrecioVenta` decimal(12,2) NOT NULL DEFAULT '0.00',
  `PrecioCosto` decimal(12,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesorios_autos`
--

CREATE TABLE `accesorios_autos` (
  `Accesorio_AutoID` int(11) NOT NULL,
  `AccesorioID` int(11) DEFAULT NULL,
  `PreventaID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autos`
--

CREATE TABLE `autos` (
  `AutoID` int(11) NOT NULL,
  `ModeloID` int(11) DEFAULT NULL,
  `ColorID` int(11) DEFAULT NULL,
  `PrecioCosto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `VIN` varchar(17) NOT NULL DEFAULT '''''',
  `Motor` varchar(30) NOT NULL DEFAULT '''''',
  `Patente` varchar(7) NOT NULL DEFAULT '''''',
  `Pedido` int(1) NOT NULL DEFAULT '0',
  `Recibido` int(1) NOT NULL DEFAULT '0',
  `FechaProgramadaDeEntrega` datetime(3) DEFAULT NULL,
  `FechaEntregaReal` datetime(3) DEFAULT NULL,
  `AnioProduccion` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `ClienteID` int(11) NOT NULL,
  `CondicionIVAID` int(11) DEFAULT NULL,
  `CUIT_CUIL` varchar(14) NOT NULL DEFAULT '''''',
  `DNI` varchar(10) NOT NULL DEFAULT '''''',
  `Nombre` varchar(50) NOT NULL DEFAULT '''''',
  `Apellido` varchar(50) NOT NULL DEFAULT '''''',
  `Direccion` varchar(50) NOT NULL DEFAULT '''''',
  `CodigoPostal` varchar(8) NOT NULL DEFAULT '''''',
  `Localidad` varchar(50) NOT NULL DEFAULT '''''',
  `Provincia` varchar(20) NOT NULL DEFAULT '''''',
  `Email` varchar(50) NOT NULL DEFAULT '''''',
  `Celular` varchar(40) NOT NULL DEFAULT '''''',
  `Telefono` varchar(40) NOT NULL DEFAULT '''''',
  `FechaDeNacimiento` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`ClienteID`, `CondicionIVAID`, `CUIT_CUIL`, `DNI`, `Nombre`, `Apellido`, `Direccion`, `CodigoPostal`, `Localidad`, `Provincia`, `Email`, `Celular`, `Telefono`, `FechaDeNacimiento`) VALUES
(1, 1, '20-17322458-6', '10000000', 'Roberto', 'Ojeda', 'Direccion 1', 'A0001AAA', 'Localidad 1', 'Provincia 1', 'nombre@apellido.com', '15-1001-1001', '1001-0110', NULL),
(2, 1, '20-17322458-6', '10000000', 'Juan Carlos', 'Mesa', 'Direccion 1', 'A0001AAA', 'Localidad 1', 'Provincia 1', 'nombre@apellido.com', '15-1001-1001', '1001-0110', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colores`
--

CREATE TABLE `colores` (
  `ColorID` int(11) NOT NULL,
  `Descripcion` varchar(50) NOT NULL DEFAULT '''''',
  `Activo` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `colores`
--

INSERT INTO `colores` (`ColorID`, `Descripcion`, `Activo`) VALUES
(1, 'Rojo', 0),
(2, 'Verde', 0),
(3, 'Negro', 0),
(4, 'Azul', 0),
(5, 'Gris', 0),
(6, 'Blanco', 0),
(7, 'Naranja', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comprobantes`
--

CREATE TABLE `comprobantes` (
  `ComprobanteID` int(11) NOT NULL,
  `PreventaID` int(11) DEFAULT NULL,
  `Documento` varchar(2) NOT NULL DEFAULT '''''',
  `Letra` char(1) NOT NULL,
  `Sucursal` smallint(6) NOT NULL DEFAULT '0',
  `Numero` int(11) NOT NULL DEFAULT '0',
  `Fecha` datetime(3) DEFAULT NULL,
  `Comentario` varchar(500) NOT NULL DEFAULT '''''',
  `Exento` decimal(12,2) NOT NULL DEFAULT '0.00',
  `Gravado` decimal(12,2) NOT NULL DEFAULT '0.00',
  `TasaDeIVA` decimal(4,2) NOT NULL DEFAULT '0.00',
  `IVA` decimal(12,2) NOT NULL DEFAULT '0.00',
  `Total` decimal(12,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `condicionesiva`
--

CREATE TABLE `condicionesiva` (
  `CondicionIVAID` int(11) NOT NULL,
  `Descripcion` varchar(52) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `condicionesiva`
--

INSERT INTO `condicionesiva` (`CondicionIVAID`, `Descripcion`) VALUES
(1, 'IVA Responsable Inscripto'),
(2, 'IVA Responsable no Inscripto'),
(3, 'IVA no Responsable'),
(4, 'IVA Sujeto Exento'),
(5, 'Consumidor Final'),
(6, 'Responsable Monotributo'),
(7, 'Sujeto no Categorizado'),
(8, 'Proveedor del Exterior'),
(9, 'Cliente del Exterior'),
(10, 'IVA Liberado â€“ Ley NÂº 19.640'),
(11, 'IVA Responsable Inscripto â€“ Agente de PercepciÃ³n'),
(12, 'PequeÃ±o Contribuyente Eventual'),
(13, 'Monotributista Social'),
(14, 'PequeÃ±o Contribuyente Eventual Social');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelos`
--

CREATE TABLE `modelos` (
  `ModeloID` int(11) NOT NULL,
  `Codigo` varchar(30) NOT NULL DEFAULT '''''',
  `Descripcion` varchar(80) NOT NULL DEFAULT '''''',
  `PrecioDeCosto` decimal(12,2) NOT NULL DEFAULT '0.00',
  `PrecioDeLista` decimal(12,2) NOT NULL DEFAULT '0.00',
  `Diesel` int(1) NOT NULL DEFAULT '0',
  `Cilindrada` double NOT NULL DEFAULT '0',
  `Activo` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modelos`
--

INSERT INTO `modelos` (`ModeloID`, `Codigo`, `Descripcion`, `PrecioDeCosto`, `PrecioDeLista`, `Diesel`, `Cilindrada`, `Activo`) VALUES
(1, 'Clio Mio', 'Expression 3P', '150000.00', '178300.00', 0, 1149, 0),
(2, 'Clio Mio', 'Confort 3P', '155000.00', '185400.00', 0, 1149, 0),
(3, 'Clio Mio', 'Confort Pack Sat 3P', '160000.00', '194800.00', 0, 1149, 0),
(4, 'Clio Mio', 'Dynamique 3P', '165000.00', '202800.00', 0, 1149, 0),
(5, 'Clio Mio', 'Confort 5P', '160000.00', '194500.00', 0, 1149, 0),
(6, 'Clio Mio', 'Confort Pack Sat 5P', '165000.00', '203600.00', 0, 1149, 0),
(7, 'Clio Mio', 'Dynamique 5P', '170000.00', '211500.00', 0, 1149, 0),
(8, 'Sandero', 'GT Line', '190000.00', '266700.00', 0, 1598, 0),
(9, 'Sandero', 'Expression', '180000.00', '222600.00', 0, 1598, 0),
(10, 'Sandero', 'Expression Pack', '185000.00', '229600.00', 0, 1598, 0),
(11, 'Sandero', 'Dynamique', '190000.00', '244700.00', 0, 1598, 0),
(12, 'Sandero', 'Privilege', '200000.00', '262700.00', 0, 1598, 0),
(13, 'Sandero', 'Privilege Pack', '205000.00', '269600.00', 0, 1598, 0),
(14, 'Sandero Stepway', 'Dynamique', '210000.00', '276500.00', 0, 1598, 0),
(15, 'Sandero Stepway', 'Privilege', '215000.00', '297500.00', 0, 1598, 0),
(16, 'Logan', 'Authentique', '180000.00', '216200.00', 0, 1598, 0),
(17, 'Logan', 'Authentique Plus', '185000.00', '222800.00', 0, 1598, 0),
(18, 'Logan', 'Expression', '190000.00', '236900.00', 0, 1598, 0),
(19, 'Logan', 'Privilege', '190000.00', '251700.00', 0, 1598, 0),
(20, 'Logan', 'Privilege Plus', '195000.00', '258300.00', 0, 1598, 0),
(21, 'Duster', 'Expression 1.6 4x2', '230000.00', '324900.00', 0, 1598, 0),
(22, 'Duster', 'Dynamique 1.6 4x2', '235000.00', '340000.00', 0, 1598, 0),
(23, 'Duster', 'Privilege 1.6 4x2', '240000.00', '360700.00', 0, 1598, 0),
(24, 'Duster', 'Privilege 2.0 4x2', '245000.00', '375400.00', 1, 1998, 0),
(25, 'Duster', 'Privilege 2.0 4x4', '250000.00', '412900.00', 1, 1998, 0),
(26, 'Fluence', 'Dynamique 1.6', '230000.00', '317800.00', 0, 1398, 0),
(27, 'Fluence', 'Dynamique Pack 1.6', '235000.00', '327900.00', 0, 1398, 0),
(28, 'Fluence', 'Luxe 1.6', '240000.00', '343300.00', 0, 1398, 0),
(29, 'Fluence', 'Luxe 2.0', '250000.00', '361400.00', 1, 1598, 0),
(30, 'Fluence', 'Luxe Pack 2.0', '255000.00', '383700.00', 1, 1598, 0),
(31, 'Fluence', 'Luxe CVT 2.0', '260000.00', '382500.00', 1, 1998, 0),
(32, 'Fluence', 'Luxe Pack Cuero', '265000.00', '392600.00', 1, 1598, 0),
(33, 'Fluence', 'Luxe Pack CVT 2.0', '265000.00', '404100.00', 1, 2003, 0),
(34, 'Fluence', 'Privilege', '275000.00', '423300.00', 1, 2003, 0),
(35, 'Megane III', 'Ph2 Luxe Pack 1.6L', '275000.00', '381400.00', 1, 1598, 0),
(36, 'Kangoo', 'Ph3 Auth. 1.6 1P', '185000.00', '274500.00', 1, 1598, 0),
(37, 'Kangoo', 'Ph3 Auth. Plus 1.6 2P', '190000.00', '294900.00', 1, 1598, 0),
(38, 'Kangoo', 'Ph3 Sportway 1.6 2P', '200000.00', '312900.00', 1, 1598, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preventas`
--

CREATE TABLE `preventas` (
  `PreventaID` int(11) NOT NULL,
  `AutoID` int(11) DEFAULT NULL,
  `ClienteID` int(11) DEFAULT NULL,
  `Numero` varchar(15) NOT NULL DEFAULT '''''',
  `Fecha` datetime(3) DEFAULT NULL,
  `PrecioDeVenta` decimal(12,2) NOT NULL DEFAULT '0.00',
  `Comentario` varchar(1000) NOT NULL DEFAULT ''''''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accesorios`
--
ALTER TABLE `accesorios`
  ADD PRIMARY KEY (`AccesorioID`);

--
-- Indices de la tabla `accesorios_autos`
--
ALTER TABLE `accesorios_autos`
  ADD PRIMARY KEY (`Accesorio_AutoID`),
  ADD KEY `AccesorioID` (`AccesorioID`),
  ADD KEY `PreventaID` (`PreventaID`);

--
-- Indices de la tabla `autos`
--
ALTER TABLE `autos`
  ADD PRIMARY KEY (`AutoID`),
  ADD KEY `ColorID` (`ColorID`),
  ADD KEY `ModeloID` (`ModeloID`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`ClienteID`),
  ADD KEY `CondicionIVAID` (`CondicionIVAID`);

--
-- Indices de la tabla `colores`
--
ALTER TABLE `colores`
  ADD PRIMARY KEY (`ColorID`);

--
-- Indices de la tabla `comprobantes`
--
ALTER TABLE `comprobantes`
  ADD PRIMARY KEY (`ComprobanteID`),
  ADD KEY `PreventaID` (`PreventaID`);

--
-- Indices de la tabla `condicionesiva`
--
ALTER TABLE `condicionesiva`
  ADD PRIMARY KEY (`CondicionIVAID`);

--
-- Indices de la tabla `modelos`
--
ALTER TABLE `modelos`
  ADD PRIMARY KEY (`ModeloID`);

--
-- Indices de la tabla `preventas`
--
ALTER TABLE `preventas`
  ADD PRIMARY KEY (`PreventaID`),
  ADD KEY `AutoID` (`AutoID`),
  ADD KEY `ClienteID` (`ClienteID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accesorios`
--
ALTER TABLE `accesorios`
  MODIFY `AccesorioID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `accesorios_autos`
--
ALTER TABLE `accesorios_autos`
  MODIFY `Accesorio_AutoID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `autos`
--
ALTER TABLE `autos`
  MODIFY `AutoID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `ClienteID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `colores`
--
ALTER TABLE `colores`
  MODIFY `ColorID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `comprobantes`
--
ALTER TABLE `comprobantes`
  MODIFY `ComprobanteID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `condicionesiva`
--
ALTER TABLE `condicionesiva`
  MODIFY `CondicionIVAID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `modelos`
--
ALTER TABLE `modelos`
  MODIFY `ModeloID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT de la tabla `preventas`
--
ALTER TABLE `preventas`
  MODIFY `PreventaID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `accesorios_autos`
--
ALTER TABLE `accesorios_autos`
  ADD CONSTRAINT `accesorios_autos_ibfk_1` FOREIGN KEY (`AccesorioID`) REFERENCES `accesorios` (`AccesorioID`),
  ADD CONSTRAINT `accesorios_autos_ibfk_2` FOREIGN KEY (`PreventaID`) REFERENCES `preventas` (`PreventaID`);

--
-- Filtros para la tabla `autos`
--
ALTER TABLE `autos`
  ADD CONSTRAINT `autos_ibfk_1` FOREIGN KEY (`ColorID`) REFERENCES `colores` (`ColorID`),
  ADD CONSTRAINT `autos_ibfk_2` FOREIGN KEY (`ModeloID`) REFERENCES `modelos` (`ModeloID`);

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`CondicionIVAID`) REFERENCES `condicionesiva` (`CondicionIVAID`);

--
-- Filtros para la tabla `comprobantes`
--
ALTER TABLE `comprobantes`
  ADD CONSTRAINT `comprobantes_ibfk_1` FOREIGN KEY (`PreventaID`) REFERENCES `preventas` (`PreventaID`);

--
-- Filtros para la tabla `preventas`
--
ALTER TABLE `preventas`
  ADD CONSTRAINT `preventas_ibfk_1` FOREIGN KEY (`AutoID`) REFERENCES `autos` (`AutoID`),
  ADD CONSTRAINT `preventas_ibfk_2` FOREIGN KEY (`ClienteID`) REFERENCES `clientes` (`ClienteID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
